import React from 'react';
import UsersPage from './Component/UsersPage/UsersPage';


function App() {

  return (
    <div className="container">
      <UsersPage />
    </div>
  );
}

export default App;
