import { FETCH_USERS, SEARCH_VALUE, GENDER_VALUE, COUNTRY_VALUE, DIRECTION_VALUE, IS_FAVORITES, FAVORITES_FILTRATION_DELETE, } from "../actions/actionTypes";


const initialState = {
    users: [],
    searchValue: '',
    isGender: '',
    isCountry: '',
    isDirection: '',
    isFavorites: [],
}

const reducerFetchUsers = (state = initialState, action) => {

    switch (action.type) {

        
        case FETCH_USERS:
            let filterNotNull = action.payload.filter(item => item.id.value !== null ? item : null);
            return {
                ...state,
                users: state.users.concat(filterNotNull)
            }

        case SEARCH_VALUE:
            return {
                ...state,
                searchValue: action.payload
            }

        case GENDER_VALUE:
            return {
                ...state,
                isGender: action.payload
            }

        case COUNTRY_VALUE:
            return {
                ...state,
                isCountry: action.payload
            }

        case DIRECTION_VALUE:
            return {
                ...state,
                isDirection: action.payload
            }

        case IS_FAVORITES:
            return {
                ...state,
                isFavorites: state.isFavorites.concat(action.payload)
            }

        case FAVORITES_FILTRATION_DELETE:
            return {
                ...state,
                isFavorites: state.isFavorites.filter(item => item.id.value.toString() !== action.payload)
            }


        default:
            return state;
    }

};

export default reducerFetchUsers;