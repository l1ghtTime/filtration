import { combineReducers } from 'redux';
import reducerFetchUsers from './reducers/reducerFetchUsers';



export default combineReducers({
    reducerFetchUsers,
});