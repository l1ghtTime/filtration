import { FETCH_USERS, SEARCH_VALUE, GENDER_VALUE, COUNTRY_VALUE, DIRECTION_VALUE, IS_FAVORITES, FAVORITES_FILTRATION_DELETE, } from "../actions/actionTypes";

export function addFetchUsers(payload){

    console.log("PAYLOAD", payload);
    return {
        type: FETCH_USERS,
        payload: payload
    }
}

export function getFetchUsers() {
    return dispatch => {
        return fetch('https://randomuser.me/api/?results=10')
            .then(response => response.json())
            .then(json => {
                json.results.map(item => item.linkStatus = true)

                dispatch(addFetchUsers(json.results))
                return json.products;
            })
    }
}


export function addSearchValue(payload){
    return {
        type: SEARCH_VALUE,
        payload: payload
    }
}

export function addGenderValue(payload){
    return {
        type: GENDER_VALUE,
        payload: payload
    }
}

export function addCountryValue(payload){
    return {
        type: COUNTRY_VALUE,
        payload: payload
    }
}

export function addDirectionValue(payload){
    return {
        type: DIRECTION_VALUE,
        payload: payload
    }
}

export function addFavorites(payload){
    return {
        type: IS_FAVORITES,
        payload: payload
    }
}

export function addFavoritesFiltration(payload){
    return {
        type: FAVORITES_FILTRATION_DELETE,
        payload: payload
    }
}


