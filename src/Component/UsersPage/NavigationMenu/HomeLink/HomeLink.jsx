import React from 'react';
import { Link } from 'react-router-dom';
import home from '../../../../image/home.svg';

const HomeLink = () => {
    return (
        <Link to="/" className="favorite">Home page
            <img src={home} alt="favorite-icon" className="favorite__image" />
        </Link>
    )
}

export default HomeLink;
