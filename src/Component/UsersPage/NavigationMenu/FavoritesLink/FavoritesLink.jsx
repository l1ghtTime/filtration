import React from 'react';
import { Link } from 'react-router-dom';
import favorites from '../../../../image/favorites.svg';

const FavoritesLink = () => {
    return (
        <Link to="/favorites" className="favorite">Favorites page
            <img src={favorites} alt="favorite-icon" className="favorite__image" />
        </Link>
    )
}

export default FavoritesLink;
