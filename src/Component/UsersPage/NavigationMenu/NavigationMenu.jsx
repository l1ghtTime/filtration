import React from 'react';
import FavoritesLink from '../NavigationMenu/FavoritesLink/FavoritesLink';
import HomeLink from './HomeLink/HomeLink';

const NavigationMenu = () => {
    return (
        <nav className="navigation-menu">
            <ul className="navigation-menu__list">
                <div className="row">

                    <li className="navigation-menu__item">
                        <HomeLink />
                    </li>


                    <li className="navigation-menu__item">
                        <FavoritesLink />
                    </li>

                </div>
            </ul>
        </nav>
    )
}

export default NavigationMenu;
