import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import box from '../../../image/box.svg';
import close from '../../../image/close.svg';
import { addFavoritesFiltration } from '../../../redux/actions/actionFetchUsers';

const FavoritesPage = () => {

    const favoritesPosts = useSelector(state => {
        let { isFavorites, searchValue, isGender, isCountry, isDirection, } = state.reducerFetchUsers;

        if(searchValue){
            isFavorites = isFavorites.filter(user => {
               return user.name.first.includes(searchValue);
            });
        }

        if(isGender) {
            isFavorites = isFavorites.filter(user => user.gender === isGender.toLowerCase());
        }

        if(isCountry) {
            if(isCountry !== 'All countries') {
                isFavorites = isFavorites.filter(user => user.location.country.includes(isCountry)); 
            }
        }

        if(isDirection === 'Z-A') {
            isFavorites = isFavorites.slice().reverse();
        }

        return isFavorites;  
    });

    const dispatch = useDispatch();

    function handleClick(event) {
        favoritesPosts.map(item => item.id.value.toString() === event.target.parentNode.id ? (item.linkStatus = true) : null)
        dispatch(addFavoritesFiltration(event.target.parentNode.id));
    }
    
    return (
        <div className="favorite-page">
            {!favoritesPosts.length ?
                <img src={box} alt="favorite-page-icon" className="favorite-page__img" />

                : <ul className="list">
                    {favoritesPosts.map(card => {
                    return (
                        <li className="list__item" key={card.id.value} id={card.id.value}>
                            <span>{card.name.first}</span>
                            <span>{card.gender}</span>
                            <span>{card.email}</span>
                            <span>{card.location.country}</span>
                            <img src={close} className="close" alt="close" onClick={handleClick}/>
                        </li>
                    )
                })}
                </ul>
            }

        </div>
    )
}

export default FavoritesPage;
