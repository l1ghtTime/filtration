import React from 'react';
import { Route, Switch } from 'react-router-dom';
import FavoritesPage from './FavoritesPage/FavoritesPage';
import NavigationMenu from './NavigationMenu/NavigationMenu';
import NavigationPanel from './NavigationPanel/NavigationPanel';
import './UsersPage.sass';
import UsersPosts from './UsersPosts/UsersPosts';

const UsersPage = () => {

    return (
        <React.Fragment>
            <NavigationPanel />
            <NavigationMenu />

            <Switch>
                <Route exact path='/' component={UsersPosts} />
                <Route path='/favorites' component={FavoritesPage} />
            </Switch>
        
        </React.Fragment>
    )
}

export default UsersPage;
