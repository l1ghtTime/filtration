import React from 'react';
import Country from './Country/Country';
import Direction from './Direction/Direction';
import Gender from './Gender/Gender';
import Search from './Search/Search';

const NavigationPanel = () => {
    return (
        <div className="navigation-panel">
            <div className="row">
                <Search />
                <Gender />
                <Country />
                <Direction />
            </div>
        </div>
    )
}

export default NavigationPanel;
