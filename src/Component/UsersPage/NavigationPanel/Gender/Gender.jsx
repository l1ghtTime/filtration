import React from 'react';
import { useDispatch } from 'react-redux';
import { addGenderValue } from '../../../../redux/actions/actionFetchUsers';

const Gender = () => {
    const dispatch = useDispatch();

    function handleChange(event) {
        dispatch(addGenderValue(event.target.value));
    }

    return (
        <div className="col-lg-3">
            <select className="navigation-panel__gender gender" onChange={handleChange}>
                <option value="">All genders</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
        </div>
    )
}

export default Gender;
