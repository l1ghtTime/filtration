import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { addCountryValue, } from '../../../../redux/actions/actionFetchUsers';



const Country = () => {
    const card = useSelector(state => state.reducerFetchUsers.users);
    const filterCard = useSelector(state => state.reducerFetchUsers.isFavorites);
    const dispatch = useDispatch();

    function handleChange(event) {
        dispatch(addCountryValue(event.target.value));
    }

    return (
        <div className="col-lg-3">
            <select className="navigation-panel__country country" onChange={handleChange}>
                <option value="">All countries</option>
                <Switch>
                    {<Route exact path='/' render={() => card.map((item, index) => <option key={index} value={item.location.country}>{item.location.country}</option>)}/>}
                    {<Route path='/favorites' render={() => filterCard.map((item, index) => <option key={index} value={item.location.country}>{item.location.country}</option>)}/>}
                </Switch>
            </select>
        </div>
    )
}

export default Country;
