import React from 'react';
import { useDispatch } from 'react-redux';
import { addDirectionValue } from '../../../../redux/actions/actionFetchUsers';

const Direction = () => {


    const dispatch = useDispatch();
    
    function handleChange(event) {
        dispatch(addDirectionValue(event.target.value));
    }

    return (
        <div className="col-lg-2">
            <select className="navigation-panel__direction direction" onChange={handleChange}>
                <option>A-Z</option>
                <option>Z-A</option>
            </select>
        </div>
    )
}

export default Direction;
