import React from 'react';
import { useDispatch } from 'react-redux';
import { addSearchValue } from '../../../../redux/actions/actionFetchUsers';

const Search = () => {

    const dispatch = useDispatch();

    function handleChange(event) {
        dispatch(addSearchValue(event.target.value));
    }

    return (
        <div className="col-lg-4">
            <input type="text" className="navigation-panel__search search" placeholder="Wright a full name..." onChange={handleChange}/>
        </div>
    )
}

export default Search;
