import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { addFavorites, getFetchUsers } from '../../../redux/actions/actionFetchUsers';


const UsersPosts = () => {

    const dispatch = useDispatch();

    const usersData = useSelector(state => state.reducerFetchUsers.users);
    
    const usersPosts = useSelector(state => {
        let { users, searchValue, isGender, isCountry, isDirection, } = state.reducerFetchUsers;

        if(searchValue){
            users = users.filter(user => {
               return user.name.first.includes(searchValue);
            });
        }

        if(isGender) {
            users = users.filter(user => user.gender === isGender.toLowerCase());
        }

        if(isCountry) {
            if(isCountry !== 'All countries') {
                users = users.filter(user => user.location.country.includes(isCountry)); 
            }
        }

        if(isDirection === 'Z-A') {
            users = users.slice().reverse();
        }

        return users;  
    });

    useEffect(() => {
        if(!usersData.length) {
            dispatch(getFetchUsers());
        }

    }, [dispatch, usersData]);

    function handleClick(event) {
        usersPosts.filter(item => item.id.value.toString() === event.target.parentNode.id ? dispatch(addFavorites(item)) && (item.linkStatus = false) : null);
    }

    return (
        <div className="row">
            <div className="col-lg-12">

                <ul className="list">
                    {
                        usersPosts.map(user => {
                            return (
                                <li className="list__item" key={user.id.value} id={user.id.value}>
                                    <span>{user.name.first}</span>
                                    <span>{user.gender}</span>
                                    <span>{user.email}</span>
                                    <span>{user.location.country}</span>
                                    {user.linkStatus ?
                                        <Link to="/" onClick={handleClick}>Add to favourites</Link>
                                        : null
                                    }
                                </li>
                            );
                        })
                    }
                </ul>

            </div>
        </div>
    )
}

export default UsersPosts;
